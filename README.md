
## Pre requisites:

- install vagrant 

- setup vagrant to work with libvirt

- recomended vagrant plugin vagrant-hostsupdater

- base on https://github.com/itwonderlab/ansible-vbox-vagrant-kubernetes

- remplaced box for libvirt image

- adapted settings for libvirt


## Down & up

```bash
vagrant destroy --force  && sudo sed -i '/k8s/d' /etc/hosts && rm -rf .vagrant && rm /tmp/daf-k8s-01-join-co*

vagrant up --no-parallel --color --timestamp
```

Use --no-parallel to get time to prepare the primary node, generate the token, and use this token in the workers.

It takes about ~15 mins to be ready.

continue based on https://www.itwonderlab.com/en/installating-kubernetes-dashboard/

you can use ssh vagrant@k8s-p-1 and run `watch kubectl get nodes`... to see the status

later, download /home/vagrant/.kube/config to some place and use it from your local:

scp vagrant@k8s-p-1:/home/vagrant/.kube/config .
mv config kubeconfig-p-1
export KUBECONFIG=$(pwd)/kubeconfig-p-1

List the pods, etc.
kubectl get pods --all-namespaces -o wide
kubectl cluster-info
kubectl cluster-info dump

## Install K8s dashboard

Find version from https://github.com/kubernetes/dashboard/releases

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml


Describe to find ip and port

kubectl -n kubernetes-dashboard describe service kubernetes-dashboard
Name:              kubernetes-dashboard
Namespace:         kubernetes-dashboard
Labels:            k8s-app=kubernetes-dashboard
Annotations:       <none>
Selector:          k8s-app=kubernetes-dashboard
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.107.143.196
IPs:               10.107.143.196
Port:              <unset>  443/TCP
TargetPort:        8443/TCP
Endpoints:         
Session Affinity:  None
Events:            <none>





kubectl delete -f https://github.com/cert-manager/cert-manager/releases/download/v1.7.1/cert-manager.yaml
kubectl delete -f https://raw.githubusercontent.com/kubevirt/hostpath-provisioner-operator/main/deploy/namespace.yaml
kubectl delete -f https://raw.githubusercontent.com/kubevirt/hostpath-provisioner-operator/main/deploy/webhook.yaml -n hostpath-provisioner
kubectl delete -f https://raw.githubusercontent.com/kubevirt/hostpath-provisioner-operator/main/deploy/operator.yaml -n hostpath-provisioner






kubectl label node worker-node01  node-role.kubernetes.io/worker=worker


(https://github.com/kubevirt/hostpath-provisioner-operator/blob/main/README.md)

# kubectl create -f https://github.com/cert-manager/cert-manager/releases/download/v1.7.1/cert-manager.yaml
kubectl create -f ./temp/cert-manager.yaml

kubectl wait --for=condition=Available -n cert-manager --timeout=120s --all deployments

# kubectl create -f https://raw.githubusercontent.com/kubevirt/hostpath-provisioner-operator/main/deploy/namespace.yaml
kubectl create -f ./temp/namespace.yaml

# kubectl create -f https://raw.githubusercontent.com/kubevirt/hostpath-provisioner-operator/main/deploy/webhook.yaml -n hostpath-provisioner
kubectl create -f ./temp/webhook.yaml -n hostpath-provisioner

# kubectl create -f https://raw.githubusercontent.com/kubevirt/hostpath-provisioner-operator/main/deploy/operator.yaml -n hostpath-provisioner
kubectl create -f ./temp/operator.yaml -n hostpath-provisioner

wait

kubectl apply -f ./sg/02-configs/02-StorageClass-retain.yaml 

k get storageclass

kubectl patch storageclass hostpath-csi -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'

# cd temp
kubectl apply -f ./temp/test.pv.yaml
kubectl get pvc myclaim1
kubectl delete -f ./temp/test.pv.yaml


helmfile -f ./sg/01-operators/helmfile.yaml apply

kubectl apply -f ./sg/02-configs/01-CreateNameSpaces.yaml 

# moved arriba
# kubectl apply -f ./sg/02-configs/02-StorageClass-retain.yaml 

kubectl apply -f ./sg/02-configs/03-SGInstanceProfile.yaml

kubectl apply -f ./sg/02-configs/04-SGPostgresConfig-14.yaml 
kubectl apply -f ./sg/02-configs/05-SGPoolingConfig.yaml


kubectl apply -f ./sg/02-configs/08-sg-cdb.yaml  

## Creating a PostgreSQL Cluster called demo-db
```
kubectl apply -f ./03-cluster/01-SGCluster-daf.yaml
```






