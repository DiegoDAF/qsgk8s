#!/bon/bash
vagrant up --no-parallel --color --timestamp

rm kubeconfig-p-1
ssh-keygen -f "/home/daf/.ssh/known_hosts" -R "k8s-p-1"
scp vagrant@k8s-p-1:/home/vagrant/.kube/config kubeconfig-p-1
chmod 0600 /home/daf/scripts/ongres/qsgk8s/kubeconfig-p-1

export KUBECONFIG=$(pwd)/kubeconfig-p-1

kubectl get nodes -o wide
