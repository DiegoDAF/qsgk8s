#IMAGE_NAME = "bento/ubuntu-20.04"
IMAGE_NAME = "peru/ubuntu-20.04-server-amd64"
IMAGE_NAME_VERSION = "20220901.01"

K8S_NAME = "daf-k8s-01"
K8S_HOST_IP = "192.168.8.1"


MASTERS_NUM = 1
MASTERS_CPU = 4 
MASTERS_MEM = 4048

NODES_NUM = 2
NODES_CPU = 6
NODES_MEM = 8048

#IP_BASE = "10.0.0."
IP_BASE = "192.168.8."


Vagrant.configure("2") do |config|
    config.ssh.insert_key = false

    (1..MASTERS_NUM).each do |i|      
        config.vm.define "k8s-p-#{i}" do |master|
            master.vm.box = IMAGE_NAME
            master.vm.box_version = IMAGE_NAME_VERSION
            master.vm.network "private_network", ip: "#{IP_BASE}#{i + 200}"
            master.vm.hostname = "k8s-p-#{i}"
            master.vm.provider :libvirt do |v|
                v.memory = MASTERS_MEM
                v.cpus = MASTERS_CPU
                v.driver = "kvm"
                v.graphics_type = "none"
                v.machine_virtual_size = 50
                v.uri = "qemu:///system"
            end
            master.vm.provision "ansible" do |ansible|
                ansible.playbook = "roles/k8s.yml"
                #Redefine defaults
                ansible.extra_vars = {
                    k8s_cluster_name:       K8S_NAME,                    
                    k8s_master_admin_user:  "vagrant",
                    k8s_master_admin_group: "vagrant",
                    k8s_master_apiserver_advertise_address: "#{IP_BASE}#{i + 200}",
                    k8s_master_node_name: "k8s-p-#{i}",
                    k8s_node_public_ip: "#{IP_BASE}#{i + 200}",
                    k8s_host_public_ip: "#{K8S_HOST_IP}"
                }                
            end
        end
    end

    (1..NODES_NUM).each do |j|
        config.vm.define "k8s-n-#{j}" do |node| 
            node.vm.box = IMAGE_NAME
            node.vm.box_version = IMAGE_NAME_VERSION
            node.vm.network "private_network", ip: "#{IP_BASE}#{j + 200 + MASTERS_NUM}"
            node.vm.hostname = "k8s-n-#{j}"
            node.vm.provider "virtualbox" do |v|
                v.memory = NODES_MEM
                v.cpus = NODES_CPU
                v.driver = "kvm"
                v.graphics_type = "none"
                v.machine_virtual_size = 50
                v.uri = "qemu:///system"
            end            

            node.vm.provision "ansible" do |ansible| 
                ansible.playbook = "roles/k8s.yml"                   
                #Redefine defaults
                ansible.extra_vars = {
                    k8s_cluster_name:     K8S_NAME,
                    k8s_node_admin_user:  "vagrant",
                    k8s_node_admin_group: "vagrant",
                    k8s_node_public_ip: "#{IP_BASE}#{j + 200 + MASTERS_NUM}",
                    k8s_host_public_ip: "#{K8S_HOST_IP}"

                }
            end
        end
    end
end
