<network>
  <name>default</name>
  <uuid>3626525f-447b-400b-ad2a-b6ed23430606</uuid>
  <forward mode='nat'/>
  <bridge name='virbr0' stp='on' delay='0'/>
  <mac address='52:54:00:ee:5a:87'/>
  <ip address='192.168.7.7' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.7.230' end='192.168.7.250'/>
    </dhcp>
  </ip>
</network>



